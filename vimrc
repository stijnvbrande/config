" Gotta be first
set nocompatible

" Vundle; VIM plugin mamager
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Vim themes
Plugin 'altercation/vim-colors-solarized'
"Plugin 'tomasr/molokai'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'

" Vim as a programmer's text editor
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'xuyuanp/nerdtree-git-plugin'
Plugin 'majutsushi/tagbar'
Plugin 'vim-syntastic/syntastic'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'junegunn/fzf.vim', { 'do': './install --bin' }
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'

" ----- Working with Git ----------------------------------------------
Plugin 'airblade/vim-gitgutter'
Plugin 'tpope/vim-fugitive'

" ----- Other text editing features -----------------------------------
Plugin 'Raimondi/delimitMate'

" ----- Extras
" Align CSV files at commas, align Markdown tables, and more
Plugin 'godlygeek/tabular'
" Copy to system clipboard
Plugin 'christoomey/vim-system-copy'
" Git syntax
Plugin 'tpope/vim-git'
" Load editorconfig files
Plugin 'editorconfig/editorconfig-vim'
Plugin 'AnsiEsc.vim'
Plugin 'JamshedVesuna/vim-markdown-preview'

call vundle#end()

filetype plugin indent on

" Remap leader from \ to ,
"nnoremap , \
" Remap : to ;
nnoremap ; :


" --- General settings ---
set backspace=indent,eol,start
set ruler
set number
set showcmd
set incsearch
set hlsearch
set history=1000
set undolevels=1000
set wildignore=*.swp,*.bak,*.pyc,*.class

" Tab configuration
set expandtab
set tabstop=2
set softtabstop=2
set shiftwidth=2
set autoindent

syntax on

" Enable mouse in VIM
"set mouse=a

" We need this for plugins like Syntastic and vim-gitgutter which put symbols
" in the sign column.
hi clear SignColumn

" Automatically deletes all trailing whitespace on save / write.
autocmd BufWritePre * %s/\s\+$//e

" Disables auttmatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Open .vimrc file from anywhere by typing :vimrc
nmap <leader>vimrc :tabe ~/.vimrc<cr>
" Autoreload VIMRC when saved
autocmd bufwritepost .vimrc source $MYVIMRC

" Toggle paste mode
set pastetoggle=<F2>

" Easy window navigation
map <C-h> <C-w>h
map <C>-<up> <C-w>h
map <C-j> <C-w>j
map <C>-<left> <C-w>j
map <C-k> <C-w>k
map <C>-<right> <C-w>k
map <C-l> <C-w>l
map <C>-<down> <C-w>l

" Trick to save when you forgot to open the file with sudo
cmap w!! w !sudo tee % >/dev/null

" Set the hidden charachters to list
"set listchars=tab:▸\ ,trail:·,extends:#,nbsp:·
" Toggle show/hide invisible chars
nnoremap <leader>i :set list!<cr>

" Format JSON
nmap =json :%!python -m json.tool<CR>
nmap =jq :%!jq -S '.'<CR>
nmap =yq :%!yq -S '.'<CR>


" Disable backups / swp files
"set nobackup
set noswapfile

" ----- Plugin-Specific Settings --------------------------------------

" ----- altercation/vim-colors-solarized settings -----
" Toggle this to "light" for light colorscheme
set background=dark

" Uncomment the next line if your terminal is not configured for solarized
let g:solarized_termcolors=256

" Set the colorscheme
colorscheme solarized

" ----- bling/vim-airline settings -----
" Always show statusbar
set laststatus=2

" Fancy arrow symbols, requires a patched font
" To install a patched font, run over to
"     https://github.com/abertsch/Menlo-for-Powerline
" download all the .ttf files, double-click on them and click "Install"
" Finally, uncomment the next line
"let g:airline_powerline_fonts = 1

" Show PASTE if in paste mode
let g:airline_detect_paste=1

" Show airline for tabs too
let g:airline#extensions#tabline#enabled = 1

" Use the solarized theme for the Airline status bar
let g:airline_theme='solarized'

" ----- jistr/vim-nerdtree-tabs -----
" Open/close NERDTree Tabs with \t
nmap <silent> <leader>t :NERDTreeTabsToggle<CR>
" To have NERDTree always open on startup
let g:nerdtree_tabs_open_on_console_startup = 0
let g:NERDTreeShowHidden=1
let g:NERDTreeWinSize = 40

" ----- scrooloose/syntastic settings -----
let g:syntastic_error_symbol = '✘'
let g:syntastic_warning_symbol = "▲"
augroup mySyntastic
  au!
  au FileType tex let b:syntastic_mode = "passive"
augroup END
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_puppet_checkers = ['puppet', 'puppetlint']
let g:syntastic_sh_checkers = ['shellcheck']
let g:syntastic_sh_shellcheck_args = "-x"

" ----- airblade/vim-gitgutter settings -----
" In vim-airline, only display "hunks" if the diff is non-zero
let g:airline#extensions#hunks#non_zero_only = 1

" --- Raimondi/delimitMate settings -----
let delimitMate_expand_cr = 1
augroup mydelimitMate
  au!
  au FileType markdown let b:delimitMate_nesting_quotes = ["`"]
  au FileType tex let b:delimitMate_quotes = ""
  au FileType tex let b:delimitMate_matchpairs = "(:),[:],{:},`:'"
  au FileType python let b:delimitMate_nesting_quotes = ['"', "'"]
augroup END

" ----- majutsushi/tagbar settings -----
" Open/close tagbar with \b
nmap <silent> <leader>b :TagbarToggle<CR>
" Uncomment to open tagbar automatically whenever possible
"autocmd BufEnter * nested :call tagbar#autoopen(0)

au BufEnter *.yaml,*.yml,*.j2,*.json set cursorcolumn

" YAML settings
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
let g:syntastic_yaml_checkers = ['yamllint']


" ----- JamshedVesuna/vim-markdown-preview settings -----
let vim_markdown_preview_github=1
let vim_markdown_preview_browser="Firefox"

" ----- JuneGunn/fzf.vim settings -----

set rtp+=~/.fzf

" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

