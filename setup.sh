#!/bin/bash -x
#
# Script to install relevant configuration

LN=$(command -v ln)
LN_OPTS="-f -s -v"

# GIT
"${LN}" ${LN_OPTS} "${PWD}/gitconfig" "${HOME}/.gitconfig"
"${LN}" ${LN_OPTS} "${PWD}/gitignore" "${HOME}/.gitignore"

## VIM
"${LN}" ${LN_OPTS} "${PWD}/vimrc" "${HOME}/.vimrc"
"${LN}" ${LN_OPTS} "${PWD}/vim/" "${HOME}/.vim"

if [ ! -d "${HOME}/.vim/bundle/Vundle.vim" ];
then
  git clone https://github.com/VundleVim/Vundle.vim.git "${HOME}/.vim/bundle/Vundle.vim"
fi

# ZSH
if [ ! -d "${HOME}/.oh-my-zsh" ];
then
  git clone https://github.com/robbyrussell/oh-my-zsh.git "${HOME}/.oh-my-zsh"
fi
"${LN}" ${LN_OPTS} "${PWD}/zsh/custom" "${HOME}/.zsh-custom"
"${LN}" ${LN_OPTS} "${PWD}/zsh/zshrc" "${HOME}/.zshrc"
"${LN}" ${LN_OPTS} "${PWD}/zsh/zlogin" "${HOME}/.zlogin"
"${LN}" ${LN_OPTS} "${PWD}/zsh/aliases.zsh" "${HOME}/.aliases.zsh"

# Mutt
mkdir "${HOME}/Github" || true
git clone https://github.com/altercation/mutt-colors-solarized.git "${HOME}/Github/mutt-colors-solarized"
"${LN}" "${LN_OPTS}" "${PWD}/muttrc" "${HOME}/.muttrc"

# FZF
git clone --depth 1 https://github.com/junegunn/fzf.git "${HOME}/.fzf"
"${HOME}/.fzf/install"

# Terminal
mkdir "${HOME}/.config" || true
"${LN}" ${LN_OPTS} "${PWD}/terminator" "${HOME}/.config/terminator"

# Font Hack
curl -L https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip -o ~/Downloads/hack.zip
mkdir -p ~/.local/share/fonts
unzip ~/Downloads/hack.zip ~/.local/share/fonts/
rm ~/Downloads/hack.zip
