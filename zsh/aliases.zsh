#!/bin/zsh

### Aliases

## cd
# Change directory commands, mostly custom
  alias cdd="cd ~/Downloads/"
  alias cdi="cd ~/Inuits/"
  alias cdg="cd ~/Gitlab/"
  alias cda="cd ~/Ingenico/ahub"

## git
# Helper aliases for Git VCS
  alias cdg="cd $(git rev-parse --show-cdup)"
  alias gst="clear; echo '--Location--'; echo ''; pwd; echo ''; echo '--Remotes--'; echo ''; git remote -v; echo ''; echo '--Branches--'; echo ''; git branch -a; echo ''; echo '--Status--'; echo ''; git status"
  alias ga="git add"
  alias gbr="git branch"
  alias gbrd="git branch -d"
  alias gbrdr="git push origin --delete"
  alias gc="git commit"
  alias gcp="git commit --patch"
  alias gco="git checkout"
  alias gd="git diff"
  alias gf="git fetch"
  alias gfp="git fetch --prune -v"
  alias gm="git merge"
  alias gps="git push"
  alias gpl="git pull"
  alias gts="git log --all --graph --decorate --oneline --simplify-by-decoration"
  alias gl="git log --name-status"
  alias gcl+="git log --pretty=format:'* %h by %an:%d %s' --abbrev-commit"
  alias gtl="git show-branch --all"
  alias gsi="git submodule init"
  alias gsu="git submodule update"
  alias pushall='for i in `git remote`; do git push $i; done;'
  alias pullall='for i in `git remote`; do git pull $i; done;'
  alias gclean='git fetch --prune; git branch --merged | grep -Ev "\*|master" | xargs git branch -d; git gc'

## Navigation
# Helper aliases for system navigation
  alias cwd="pwd | sed 's/ /\\ /g'"

## ls
# List files and directories
  alias ll="clear; pwd; ls -lGHa"
  alias ll+="pwd; ls -aFGhlOTs"
  alias ll-="clear; ls -1aF && echo ''"
  alias lsd="ls -d1 */"
  alias ft="ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/'"
  alias ftg="ls -R | grep ":$" | sed -e 's/:$//' -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/' | grep"

## Grep
  alias grep-clean="grep -Ev '^$|^[[:blank:]]*#'"

## vagrant
# Helper aliases for vagrant
  alias vafy='~/workspace/WBE_vagrant/vafy.sh ~/workspace/WBE_vagrant'
  alias vup="vagrant up"
  alias vssh="vagrant ssh"
  alias vha="vagrant halt"
  alias vsu="vagrant suspend"
  alias vst="clear && echo '# Version' && echo '' && vagrant version | grep  Version && echo '' && echo '# Status' && echo '' && vagrant global-status && echo '--------------------------------------------------------------------' && vagrant status && echo ''"
  alias vde="vagrant destroy --force"
  alias vdef="vagrant destroy -f" # destroy a vagrant instance the don't give a sh!t way
  alias vpr="vagrant provision"

## Virtualbox
# Helper aliases for Virtualbox
  alias vbl="VBoxManage list vms"
  alias vbla="VBoxManage list runningvms"
  alias vblg="VBoxManage list vms | grep"

## zsh
# Helper aliases for zsh
  alias sz="clear && source ~/.zshrc"
  alias zwiz="autoload -Uz zsh-newuser-install; zsh-newuser-install -f"


## Ingenico
  alias ingenico-sshuttle="sshuttle -r stekke@proxy.inuits.eu:443 proxy.inuits.eu xmpp.inuits.eu gitlab.com github.com redmine.inuits.eu"
  alias bamboo-cleanup="parallel-ssh -i -H 'bamboo-gs-agent1 bamboo-gs-agent2 bamboo-gs-agent3 bamboo-gs-agent4 bamboo-gs-agent5 bamboo-gs-agent6 bamboo-gs-agent7 bamboo-gs-agent8 bamboo-gs-agent9 bamboo-gs-agent10 bamboo-gs-agent11 bamboo-gs-agent12 bamboo-gs-agent13 bamboo-gs-agent14' 'sudo find /home/bamboo/agent-bamboo-gs-agent*-home/temp/log_spool -type f -mtime +5 -exec rm {} \;'"
### Release
  alias ahub-release-versions='~/Ingenico/script-tools/ah-release-notes.sh --microservice-yaml ~/Ingenico/puppet/modules/ah_microservice/data/default.yaml --functional-yaml ~/Ingenico/puppet/modules/ah_mgmt/data/default.yaml --update-versions'
  alias pulp-curl-lab="curl -sS http://pulp.sys.lab.ingenico.com/pulp/repos/el/7/x86_64/ingenico-app/Packages/a/ | sed -e 's/.*\">//' -e 's/<\/a.*$//' | grep -Ev '^$' | grep -E '^ah-.*'"
  alias docker-puppet-module="${HOME}/Ingenico/ahub/puppet/tools/docker-module-testing/run.sh --path '${HOME}/Ingenico/ahub/puppet/modules'"
  alias git-mr="${HOME}/Ingenico/ahub/code/tools/script-tools/gitlab-push-mr.sh"

# Ubuntu
## Reload audio
  alias audio-reload='pulseaudio -k && sudo alsa force-reload'
## Gnome
  alias gnome-screenlock-disable="gsettings set org.gnome.desktop.session idle-delay 0"
  alias gnome-screenlock-enable="gsettings set org.gnome.desktop.session idle-delay 120"
